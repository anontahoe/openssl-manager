#!/bin/env python3.4

#   Copyright (C) 2015  anontahoe
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA


# todo:
#	courier-imap: dhparams -> privkey -> cert -> intermediate -> root

import os
from subprocess import call
from subprocess import check_output
from subprocess import Popen
import subprocess
import re
import fileinput
import shutil

class SslFilesStruct():
	def __init__(self, name="name", tag="tag", cipher="ecdsa", encrypted="aes"):
		self.Reset(name, tag, cipher, encrypted)

	def __str__(self):
		return self.basename+" <filelist>"

	def Reset(self, name, tag, cipher, encrypted):
		if tag != "":
			tag = "." + tag
		if encrypted != "":
			encrypted = "." + encrypted
		self.baseName = name+tag+"."+cipher
		self.keyFile = "./private/"+self.baseName+encrypted+".key.pem"
		self.csrFile = "./"+name+"/"+self.baseName+".csr.pem"
		self.certFile = "./"+name+"/"+self.baseName+".crt.pem"
		self.chainFile = "./"+name+"/"+self.baseName+".chain.pem"
		self.crlFile = "./"+name+"/"+self.baseName+".crl.pem"
		self.crlDerFile = "./"+name+"/"+self.baseName+".crl.der"
		self.crlChainFile = "./"+name+"/"+self.baseName+".crl-chain.pem"
		self.dhFile = "./"+name+"/"+self.baseName+".dhparams.pem"
		self.dhExportFile = "./"+name+"/"+self.baseName+".dhexport.pem"

class KeyStruct():
	def __init__(self, name, tag="tag", cipher="ecdsa", encrypted="", keyFileName=""):
		self.Reset(name, tag, cipher, encrypted, keyFileName)

	def __str__(self):
		return self.files.keyFile

	def Reset(self, name, tag="tag", cipher="ecdsa", encrypted="", keyFileName=""):
		if keyFileName != "":
			self.ExtractKeyDetails(name, keyFileName)
		else:
			self.tag = tag
			self.cipher = cipher
			self.encrypted = encrypted
		self.files = SslFilesStruct()
		self.files.Reset(name, self.tag, self.cipher, self.encrypted)
		print("		name:	", name)
		print("		tag:	", self.tag)
		print("		cipher:	", self.cipher)
		print("		encr.:	", self.encrypted)

	def ExtractKeyDetails(self, name, keyFileName):
		print("		Extracting key details for "+keyFileName+"...")
		regKeyDetailsEx = re.compile(name+"(\.(.+))?(\.(ecdsa|rsa))(\.(aes))?\.key\.pem")				# "name.tag.ecdsa.aes.key.pem"
		regKeyDetailsMatch = regKeyDetailsEx.match(keyFileName)
		if regKeyDetailsMatch is None:
			print("	Error: regex can't extract details from key")
		self.tag = "" if regKeyDetailsMatch.group(2) is None else regKeyDetailsMatch.group(2)			# lmao
		self.cipher = "" if regKeyDetailsMatch.group(4) is None else regKeyDetailsMatch.group(4)
		self.encrypted = "" if regKeyDetailsMatch.group(6) is None else regKeyDetailsMatch.group(6)

class SslStruct():
	def __init__(self, name="name", sslKeys=[], currentKey=None):
		self.Reset(name, sslKeys, currentKey)

	def __str__(self):
		return self.name

	def Reset(self, name, sslKeys=[], currentKey=None):
		self.name = name
		self.keyList = sslKeys
		self.currentKey = currentKey

	def AddKeys(self, keyFiles):
		for keyFile in keyFiles:
			self.keyList.append(KeyStruct(name=self.name, keyFileName=keyFile))

class FolderIndexStruct():
	def __init__(self, caList=[], serverList=[]):
		self.Reset(caList, serverList)

	def Reset(self, caList=[], serverList=[]):
		self.caList = caList
		self.serverList = serverList

def IndexAll():
	global folderIndex
	folderIndex.Reset([], [])

	directories = next(os.walk('.'))[1]
	for directory in directories:
		if directory == "private":
			continue
		print("scanning directory", directory)
		sslStructToAdd = SslStruct()
		sslStructToAdd.Reset(name=directory, sslKeys=[])
		if os.path.isdir("./"+sslStructToAdd.name+"/certs"):
			folderIndex.caList.append(sslStructToAdd)
		else:
			folderIndex.serverList.append(sslStructToAdd)
		keys = next(os.walk('private'))[2]
		matchingKeys = FilterList(keys, sslStructToAdd.name)
		print("	matching keys:", matchingKeys)
		if len(matchingKeys):
			sslStructToAdd.AddKeys(matchingKeys)


def FilterList(listToFilter, filterToApply):
	return [ listItem for listItem in listToFilter if listItem.startswith(filterToApply+".") ]

def TryDeleteFile(fileToDelete):
	try:
		os.remove(fileToDelete)
	except OSError:
		pass

def TryMoveFile(source, dest):
	try:
		shutil.move(source, dest)
	except OSError:
		pass

def TryCreateFolder(path):
	try:
		os.makedirs("./"+path)
	except OSError:
		if not os.path.isdir(path):
			raise

def TryCreateFolders(target):
	TryCreateFolder("./private")
	TryCreateFolder(str(target.name))

def TryCreateCaCertsFolder(target):
	certsDir = "./"+target.name+"/certs"
	if os.path.isdir(certsDir):
		return

	print("	Creating certs folder in ./"+certsDir+"...")
	TryCreateFolder(certsDir)
	caIndexAttrFile = open(certsDir+"/index", "w")
	caIndexAttrFile.close()
	caIndexAttrFile = open(certsDir+"/index.attr", "w")
	caIndexAttrFile.close()
	with open(certsDir+"/crlnumber", "w") as caCrlNumberFile:
		caCrlNumberFile.write("100001")
	with open(certsDir+"/serial", "w") as caSerialFile:
		caSerialFile.write("100001")

def ChooseOrCreate(question, options, alternativeOption="create new..."):
	print(question)
	for i in range(0, len(options)):
		print("	", i, "-", str(options[i]))
	print("	", len(options), "-", alternativeOption)
	return int(input("	Enter number: "))

def SelectServerOrCA(toBeDoneWhat="worked on", alternativeOption="return", listToSelectFrom=[]):
	if listToSelectFrom == []:
		listToSelectFrom = folderIndex.caList+folderIndex.serverList
	sslStruct = None
	selectionNum = ChooseOrCreate("Select server or CA to be "+toBeDoneWhat+":", listToSelectFrom, alternativeOption)
	if selectionNum == len(listToSelectFrom):
		return None
	sslStruct= listToSelectFrom[selectionNum]
	#CreateKey(sslStruct)
	return sslStruct

def AskKeyDetails(target):
	tag = input("	Enter tag: ")
	cipher = input("	Enter cipher (ecdsa/rsa): ")
	if cipher == "":
		cipher = "ecdsa"
	encrypt = input("	Encrypt private key? (y/n): ")
	if encrypt == "n" or encrypt == "no":
		encrypted = ""
	else:
		encrypted = "aes"
	return KeyStruct(target.name, tag, cipher, encrypted)

def CreateKey(target=None):
	if target == None:
		target = SelectServerOrCA("given a new key", "create new")
		if target == None:
			target = SslStruct()
			target.Reset(name="", sslKeys=[])
			target.name = input("	Enter name: ")

	TryCreateFolders(target)

	selectionNum = ChooseOrCreate("	Select key for "+target.name+" or create new one:", target.keyList)
	if selectionNum < len(target.keyList):
		target.currentKey = target.keyList[selectionNum]
		print("		using existing key.")
		return

	keyStructToAdd = AskKeyDetails(target)
	target.keyList.append(keyStructToAdd)
	target.currentKey = keyStructToAdd

	# creating new key means the old cert is invalid. also we need this for an if clause in createca() and createserver()
	TryMoveFile(keyStructToAdd.files.certFile, keyStructToAdd.files.certFile+"_keyDeleted")
	TryDeleteFile(GetSymlinkName(target, "x509"))
	TryDeleteFile(GetSymlinkName(target, "crl"))


	print("	creating key "+str(keyStructToAdd))
	if keyStructToAdd.cipher == "ecdsa":
		if keyStructToAdd.encrypted != "":
			key = check_output(["openssl", "ecparam", \
									"-genkey", \
									"-name", "secp256k1"], \
								universal_newlines=1)
			keypipe = Popen(["openssl", "ec", \
								"-aes-256-cbc", \
								"-out", keyStructToAdd.files.keyFile], \
							universal_newlines=True, stdin=subprocess.PIPE)
			keypipe.communicate(input=key)
		else:
			call(["openssl", "ecparam", \
						"-genkey", \
						"-name", "secp256k1", \
						"-out", keyStructToAdd.files.keyFile])
	else:
		rsaKeySize = "8192"
		if keyStructToAdd.encrypted != "":
			call(["openssl", "genrsa", \
						"-out", keyStructToAdd.files.keyFile, \
						"-aes256", \
						rsaKeySize])
		else:
			call(["openssl", "genrsa", \
					"-out", keyStructToAdd.files.keyFile, \
					rsaKeySize])

	#IndexAll()

def ConcatenateFiles(file1, file2, outFile):
	with open(file1, "r") as fileIn:
		chainText = fileIn.read()
	with open(file2, "r") as fileIn:
		chainText += fileIn.read() + "\n"
	print(chainText)
	with open(outFile, "w") as fileOut:
		fileOut.write(chainText)

def CreateChainFiles(caToCreate, signingCa):
	if os.path.isfile(signingCa.currentKey.files.chainFile):
		chainToCopy = signingCa.currentKey.files.chainFile
		crlChainToCopy = signingCa.currentKey.files.crlChainFile
	else:
		chainToCopy = signingCa.currentKey.files.certFile
		crlChainToCopy = signingCa.currentKey.files.crlFile

	ConcatenateFiles(caToCreate.currentKey.files.certFile, chainToCopy, caToCreate.currentKey.files.chainFile)
	ConcatenateFiles(caToCreate.currentKey.files.crlFile, crlChainToCopy, caToCreate.currentKey.files.crlChainFile)

def CreateCA():
	caToCreate = SslStruct()
	caToCreate.Reset(name="", sslKeys=[])
	selectionNum = ChooseOrCreate("	Select CA or create new one:", folderIndex.caList)
	if selectionNum == len(folderIndex.caList):
		caToCreate.name = input("	Enter name: ")
		folderIndex.caList.append(caToCreate)
	else:
		caToCreate = folderIndex.caList[selectionNum]

	CreateKey(caToCreate)

	TryCreateCaCertsFolder(caToCreate)

	print("	checking if CA already exists at", caToCreate.currentKey.files.certFile)
	if os.path.isfile(caToCreate.currentKey.files.certFile):
		print("		using existing certificate")
		return caToCreate

	DisableAdditionalDomains(caToCreate)

	daysValid = int(input("	Days the certificate is valid for: "))
	AskCrlDistributionPoints(caToCreate)

	selectionNum = ChooseOrCreate("	Select signing CA:", folderIndex.caList, "selfsigned")
	if selectionNum == len(folderIndex.caList):
		# selfsigned
		result = call(["openssl", "req", \
							"-x509", \
							"-new", \
							"-config", configFile, \
							"-key", caToCreate.currentKey.files.keyFile, \
							"-out", caToCreate.currentKey.files.certFile, \
							#"-text", \
							"-days", str(daysValid), \
							"-sha512"])
		print(result)
		if result != 0:
			return None
		CreateCRL(caToCreate, None, True)
		#IndexAll()
		return caToCreate

	signingCa = folderIndex.caList[selectionNum]
	CreateKey(signingCa)

	print("	Creating ca certificate request...")
	result = call(["openssl", "req", \
							"-new", \
							"-config", configFile, \
							"-key", caToCreate.currentKey.files.keyFile, \
							"-out", caToCreate.currentKey.files.csrFile, \
							"-sha512"])
	print(result)
	if result != 0:
		return None

	print ("Signing certificate request...")
	result = call(["openssl", "ca", \
						"-config", "../"+configFile, \
						"-cert", "../"+signingCa.currentKey.files.certFile, \
						"-keyfile", "../"+signingCa.currentKey.files.keyFile, \
						"-extensions", "intermediate_extensions", \
						"-policy", "signing_policy", \
						"-in", "../"+caToCreate.currentKey.files.csrFile, \
						"-out", "../"+caToCreate.currentKey.files.certFile, \
						"-md", "sha512", \
						"-days", str(daysValid)], \
					cwd="./"+signingCa.name)
	print(result)
	if result != 0:
		return None

	CreateCRL(signingCa, None)
	CreateCRL(caToCreate, signingCa)
	#IndexAll()
	return caToCreate

def OpenSslSetVariable(variable, enabled, value=""):
	if enabled == False:
		enabledText="not"
		enabledComment="# "
	else:
		enabledText = ""
		enabledComment=""

	print("	"+enabledText, "using", variable, "=", value)
	enableDomainsRe = re.compile(r"^(#*)?(\s*?)?("+variable+r"\s+=)(\s*.*)$")
	replacedConfig = ""

	for line in fileinput.input(configFile):
		line = enableDomainsRe.sub(r""+enabledComment+r"\3 "+value, line)
		replacedConfig += line
	fileinput.close()
	with open(configFile, "w") as configFileHandle:
		configFileHandle.write(replacedConfig)

def EnableAdditionalDomains(target):
	OpenSslSetVariable("subjectAltName", True, "@alternate_names_"+target.name+target.currentKey.tag)

def DisableAdditionalDomains(target):
	OpenSslSetVariable("subjectAltName", False, "@alternate_names_"+target.name+target.currentKey.tag)

def AskAdditionalDomains(target):
	targetNameTagString = "alternate_names_"+target.name+target.currentKey.tag
	serviceExistsRe = re.compile("\s*\[\s*"+targetNameTagString+"\s*\]\s*")
	with open(configFile, "r") as configFileHandle:
		configText = configFileHandle.read()
		serviceExistsMatch = serviceExistsRe.search(configText)
		print("	Additional domains already configured?", serviceExistsMatch)

	if serviceExistsMatch != None:
		print("	Using preconfigured additional domains.")
		EnableAdditionalDomains(target)
		return True

	print("	Enter additional domains or an empty line to stop:")
	domainList = []
	domainToAdd = input("	")
	while domainToAdd != "":
		domainList.append(domainToAdd)
		domainToAdd = input("	")

	if domainList == []:
		DisableAdditionalDomains(target)
		return False

	with open(configFile, "a") as configFileHandle:
		configFileHandle.write("\n")
		configFileHandle.write("[ "+targetNameTagString+" ]\n")
		i = 1
		for domainToAdd in domainList:
			configFileHandle.write("DNS."+str(i)+"					= "+domainToAdd+"\n")
			i += 1

	EnableAdditionalDomains(target)
	return True

def AskCrlDistributionPoints(target):
	crlDistributionUrl = input("	Enter CRL distribution URL (optional)\n		e.g. https://example.com: ")
	if crlDistributionUrl == "":
		useCrl = False
	else:
		useCrl = True
	OpenSslSetVariable("crlDistributionPoints", useCrl, "URI:"+crlDistributionUrl+"/"+target.currentKey.files.crlFile)	# i think IE needs the CRL in DER format so let's use PEM format instead.

def CreateServerCertificate():
	serverToCreate = SslStruct()
	serverToCreate.Reset(name="", sslKeys=[])
	selectionNum = ChooseOrCreate("Select server or create new one:", folderIndex.serverList)
	if selectionNum == len(folderIndex.serverList):
		serverToCreate.name = input("Enter name: ")
	else:
		serverToCreate = folderIndex.serverList[selectionNum]

	print(serverToCreate.name)

	CreateKey(serverToCreate)

	print("	checking if server already exists at", serverToCreate.currentKey.files.certFile)
	if os.path.isfile(serverToCreate.currentKey.files.certFile):
		return serverToCreate

	useAdditionalDomains = AskAdditionalDomains(serverToCreate)
	daysValid = int(input("	Days the certificate is valid for: "))
	result = call(["openssl", "req", \
						"-new", \
						"-config", configFile, \
						"-key", serverToCreate.currentKey.files.keyFile, \
						"-out", serverToCreate.currentKey.files.csrFile, \
						"-sha512"])
	print(result)
	if result != 0:
		return None

	print("	Select signing CA:")
	signingCa = CreateCA()

	if useAdditionalDomains:
		EnableAdditionalDomains(serverToCreate)

	result = call(["openssl", "ca", \
						"-config", "../"+configFile, \
						"-cert", "../"+signingCa.currentKey.files.certFile, \
						"-keyfile", "../"+signingCa.currentKey.files.keyFile, \
						"-extensions", "signing_req", \
						"-policy", "signing_policy", \
						"-in", "../"+serverToCreate.currentKey.files.csrFile, \
						"-out", "../"+serverToCreate.currentKey.files.certFile, \
						"-notext", \
						"-md", "sha512", \
						"-days", str(daysValid)], \
					cwd="./"+signingCa.name)
	print(result)
	if result != 0:
		TryDeleteFile(serverToCreate.currentKey.files.certFile)
		return None
	IndexAll()

	return serverToCreate

def CreateDhParams():
	target = CreateServerCertificate()
	if target == None:
		return

	print("	Creating DH params...")
	result = call(["openssl", "dhparam", \
						"-out", target.currentKey.files.dhFile, \
						"8192"])
	print(result)
	result = call(["openssl", "dhparam", \
						"-out", target.currentKey.files.dhExportFile, \
						"1024"])
	print(result)

	if not os.path.isfile(target.currentKey.files.certFile):
		return

	appendDhParams = input("Append DH params to certificate file "+target.currentKey.files.certFile+"? (y/n): ")
	if appendDhParams != "y":
		return

	ConcatenateFiles(target.currentKey.files.certFile, target.currentKey.files.dhFile, target.currentKey.files.certFile)

def GetSymlinkName(target, crtOrCrl):
	if crtOrCrl == "x509":
		targetFile = target.currentKey.files.certFile
		symlinkExtension = ".r0"
	elif crtOrCrl == "crl":
		targetFile = target.currentKey.files.crlFile
		symlinkExtension = ".0"

	if not os.path.isfile(targetFile):
		return ""

	hash = check_output(["openssl", crtOrCrl, \
							"-hash", \
							"-noout", \
							"-in", targetFile], \
						universal_newlines=1)
	hash = hash[:-1]	# remove linebreak
	symlinkName = "./"+target.name+"/"+hash+symlinkExtension
	return symlinkName

def CreateSymlinks(target):
	# get hash of certificate subject name
	symlinkName = GetSymlinkName(target, "x509")
	# instead of deleting the original symlink we should count up (.1, .2, .3,..)
	TryDeleteFile(symlinkName)
	os.symlink(target.currentKey.files.baseName+".crt.pem", symlinkName)

	# get hash of crl issuer name, should be the same as certSubjectHash
	symlinkName = GetSymlinkName(target, "crl")
	TryDeleteFile(symlinkName)
	os.symlink(target.currentKey.files.baseName+".crl.pem", symlinkName)

def CreateCRL(target=None, signingCa=None, selfsigned=False):
	if target == None:
		target = CreateCA()

	TryCreateCaCertsFolder(target)

	if signingCa == None and selfsigned == False:
		print("	Choose the CA and key that signed "+target.currentKey.files.certFile+" (needed to properly create the crl chain file):")
		signingCa = CreateCA()

	print("Creating CRL for", target.name)
	result = call(["openssl", "ca",\
					"-config", "../"+configFile,\
					"-gencrl",\
					"-keyfile", "../"+target.currentKey.files.keyFile,\
					"-cert", "../"+target.currentKey.files.certFile,\
					"-out", "../"+target.currentKey.files.crlFile,\
					"-md", "sha512"], cwd="./"+target.name)
	print("	", result)
	#if result != 0:
	#	return None

	print("	 converting CRL from PEM to DER")
	result = call(["openssl", "crl",\
							"-in", target.currentKey.files.crlFile,\
							"-inform", "PEM",\
							"-outform", "DER",\
							"-out", target.currentKey.files.crlDerFile])
	print("	", result)
	#if result != 0:
	#	return None


	# create symlinks and crl chain files for crt and crl
	if signingCa != None and signingCa != target:
		CreateChainFiles(target, signingCa)
	CreateSymlinks(target)

def RevokeCert():
	sslStructToRevoke = SelectServerOrCA("revoked")
	if sslStructToRevoke == None:
		return
	CreateKey(sslStructToRevoke)

	print("Select CA to revoke the cert with:")
	print("	Note: If you select the wrong CA here you will fuck up the database in the revoking CA's \"certs\" folder")
	print("	If that happens just edit the certs/index file or delete the certs folder or everything.")
	signingCa = CreateCA()

	print("\nRevoking certificate...")
	result = call(["openssl", "ca",\
						"-config", "../"+configFile, \
						"-revoke", "../"+sslStructToRevoke.currentKey.files.certFile, \
						"-keyfile", "../"+signingCa.currentKey.files.keyFile, \
						"-cert", "../"+signingCa.currentKey.files.certFile], \
					cwd="./"+signingCa.name)
	print(result)
	if result != 0:
		return

	TryMoveFile(sslStructToRevoke.currentKey.files.certFile, sslStructToRevoke.currentKey.files.certFile+"_revoked")

	CreateCRL(signingCa)
	if sslStructToRevoke in folderIndex.caList:
		CreateCRL(sslStructToRevoke, signingCa)

def Verify():
	print("NOTE: crl chains are not properly updated yet which means sometimes certs aren't properly checked for revocation.")
	print("This will be fixed in 2016 when the current certificates run out.")
	sslStructToValidate = SelectServerOrCA("verified")
	if sslStructToValidate == None:
		return
	CreateKey(sslStructToValidate)

	print("Select trusted root CA:")
	trustedCa = CreateCA()

	selectionNum = ChooseOrCreate("Select intermediate CA", folderIndex.caList, "no intermediate CA")
	if selectionNum == len(folderIndex.caList):
		intermediateCa = None
	else:
		intermediateCa = folderIndex.caList[selectionNum]
		CreateKey(intermediateCa)

	#echo "openssl verify -CAFile \"./$rootFolderName/$rootBaseName$rootTag$cipher.crt.pem\" -untrusted \"./$caFolderName/$caBaseName$caTag$cipher.chain.pem\" \"./$folderName/$baseName$tag$cipher.crt.pem\""
	if intermediateCa != None:
		result = call(["openssl", "verify",\
								"-CAfile", trustedCa.currentKey.files.certFile,\
								"-untrusted", intermediateCa.currentKey.files.chainFile,\
								"-CRLfile", intermediateCa.currentKey.files.crlChainFile, \
								"-crl_check_all", \
								sslStructToValidate.currentKey.files.certFile])
	else:
		result = call(["openssl", "verify",\
								"-CAfile", trustedCa.currentKey.files.certFile, \
								"-CRLfile", trustedCa.currentKey.files.crlFile, \
								"-crl_check_all", \
								sslStructToValidate.currentKey.files.certFile])
	print(result)

def ShutDown():
	print("shutting down...")
	exit()

commandDelegates = {0 : CreateKey,
           1 : CreateCA,
           2 : CreateServerCertificate,
		   3 : CreateDhParams,
           4 : CreateCRL,
           5 : RevokeCert,
           6 : Verify,
}


configFile = "./openssl.cnf"
global folderIndex
folderIndex = FolderIndexStruct()
TryCreateFolder("./private")
IndexAll()

print("Hello World!")

commandList = [ "key", "CA", "server", "DH params", "CRL", "revoke", "verify" ]
while True:
	selectedCommand = ChooseOrCreate("What do you want to create?", commandList, "exit")
	if selectedCommand == len(commandList):
		ShutDown()
	commandDelegates[selectedCommand]()













