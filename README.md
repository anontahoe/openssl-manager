OpenSSL-Manager
===============

This script lets you easily create your own certificate authority and certs.
If you are serious you should get a CAcert signed certificate or a commercial one. See below for instructions to create a certificate signing request with OpenSSL-Manager.

#### Features (some are still work in progress)
*	Create root and intermediate CAs
*	Create and update CRLs (WIP)
*	Create server certificates for one or multiple domains
*	choose between ECDSA and RSA and create custom DHparams
*	Combine certificates, keys and dhparams into single .pem files as needed by various applications like apache, postfix, etc. (WIP)
*	verify validity of certificates and certificate chains (WIP i think)

#### How to download
*	https: git clone https://gitlab.com/anontahoe/openssl-manager.git
*	ssh: see [here][] for instructions

#### Dependencies
*	Python 3.x

#### How to use:
>$ ./openssl-manager.py

Generally you get a list of options and you enter a number or string depending on the question and your intentions.

There is no tutorial yet but the code should be mostly straightforward so just ctrl-f through the .py file and you will have your documentation, and parts of the interface are not totally unintuitive.
Additionally you can send an email to anontahoe(at)anonmail.pwnz.org and I'll be happy to help.

[here]: https://meshnet.pwnz.org/tahoe/deploy-key.html
